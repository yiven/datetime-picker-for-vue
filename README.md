# datetime-picker-for-vue

## Project setup/安装
```
npm install
```

### Compiles and hot-reloads for development/开发模式运行
```
npm run serve
```

### Compiles and minifies for production/生产编译
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration/自定义配置
See [Configuration Reference](https://cli.vuejs.org/config/).
