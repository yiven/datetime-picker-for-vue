import Vue from 'vue'
import VueRouter from 'vue-router'
import pickerDemo from '@/views/Demo.vue'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'picker',
      component: pickerDemo
    }
  ]
})
